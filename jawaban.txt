// soal 1
create database myshop;
//soal 2
create table users(id int auto_increment,name varchar(255),email varchar(255),password varchar(255),primary key (id) );
create table categories(id int auto_increment primary key,name varchar(255));
create table items(id int auto_increment primary key,name varchar(255),description varchar(255),price int,stok int,category_id int) ;
alter table items add foreign key (category_id) references categories(id);
//soal 3
insert into users(name,email,password)
values("John Doe","John@doe.com","john123"),("Jane Doe","jane@doe.com","jenita123");
insert into categories(name)
values("gadget"),("cloth"),("men"),("women"),("branded");
insert into items(name,description,price,stok,category_id)
values("Sumsang b50","hape keren dari merek sumsang",4000000,100,1),
("Uniklooh","baju keren dari brand ternama",500000,50,2),
("IMHO Watch","jam tangan anak yang jujur banget",2000000,10,1);
//soal 4
a.
select id,email,name from users;
b.
select * from items where price >1000000;
select * from items where name like '%Uniklo%';
c. 
select items.name,items.description,items.price,items.stok,items.category_id,categories.name 
from items inner join categories on items.category_id=categories.id;
5.
update items set price = 2500000 where name like '%sumsang%';